/* 
	Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/
const getBoardInformation = require("./problem1")
const getLists = require("./problem2")
const getCards = require("./problem3")




function getDetails(boards,lists,cards,boardID,listName){
    return new Promise( (resolve,reject) => {
        setTimeout(()=>{
            data = {}
            getBoardInformation(boards,boardID)
            .then((response)=>{
                data['Board Information'] = response
                return getLists(lists,boardID)
            })
            .then((response)=>{
                data['lists'] = response
                let cardId = response.find((list)=>list.name == listName).id
                return getCards(cards,cardId)
            })
            .then((response)=>{
                data['cards'] = response
                resolve(data)
            })
            .catch((error)=>{
                reject(error)
            })
    
        },2000)
    })
}


module.exports = getDetails