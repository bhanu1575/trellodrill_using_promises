/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. 
    Then pass control back to the code that called it by using a callback function.
*/
function getCards(cards,id){
  return new Promise((resolve,reject) => {
    setTimeout(()=>{
      let results = cards[id] ? cards[id] : []
      if(results.length === 0) {
        reject('Invalid ID / No cards were found with this given ID.')
      }
      resolve(results)
    },2000)
  })
}


module.exports = getCards