/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. 
        Then pass control back to the code that called it by using a callback function.
*/
const lists = require('../lists_1.json')
const getLists = require('../problem2')

const id = 'mcu453ed'


try {
    getLists(lists,id)
    .then((response) => {
        console.log(response)
    })
    .catch((error)=>{
        console.log(error)
    })
} catch (error) {
    console.log(error)
}
