/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json 
    and then pass control back to the code that called it by using a callback function.
*/
const boards = require('../boards.json')
const getBoardInformation = require('../problem1')


const id = 'mcu453ed';

 

try {
    getBoardInformation(boards,id)
    .then((response)=>{
        console.log(response)
    })
    .catch((error)=>{
        console.error(error.message)
    })
} catch (error) {
    console.error(error);
}