/* 
	Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/
const boards = require('../boards.json')
const lists = require('../lists_1.json')
const cards = require('../cards.json')
const getDetails = require('../problem5')


try {
    getDetails(boards,lists,cards,'mcu453ed','Mind','Space')
    .then((response)=>{
        console.log(response);
    })
    .catch((error)=>{
        console.log(error);
    })
} catch (error) {
    console.log(error);
}

