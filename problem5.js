/* 
	Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/
const getBoardInformation = require("./problem1")
const getLists = require("./problem2")
const getCards = require("./problem3")




function getDetails(boards,lists,cards,boardID,listName1,listName2){
    return new Promise((resolve,reject)=>{
        data = {};
        setTimeout(()=>{
            getBoardInformation(boards,boardID)
            .then((response)=>{
                data['Board Information'] = response
                return getLists(lists,boardID)
            })
            .then((response)=>{
                let cardIds = response.filter( (list) => list.name == listName1 || list.name == listName2)
                data['lists'] = response
                return Promise.allSettled(cardIds.map( (card) =>  getCards(cards,card.id) ))
            })
            .then((response)=>{
                data['cards'] = response.map( currentValue => currentValue.value)
                resolve(data)
            })
            .catch((error)=>{
                reject(error)
            })
        },2000)
    })
}


module.exports = getDetails