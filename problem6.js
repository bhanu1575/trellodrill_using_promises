/* 
	Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/
const getBoardInformation = require("./problem1")
const getLists = require("./problem2")
const getCards = require("./problem3")



function getDetails(boards,lists,cards,boardID){
    return new Promise((resolve,reject)=>{
        data = {};
        setTimeout(()=>{
            getBoardInformation(boards,boardID)
            .then((response)=>{
                data['Board Information'] = response
                return getLists(lists,boardID)
            })
            .then((response)=>{
                data['lists'] = response
                return Promise.allSettled(response.map( (list) =>  getCards(cards,list.id) ))
            })
            .then((response)=>{
                data['cards'] = response.map( currentValue => currentValue.value)
                resolve(data)
            })
            .catch((error)=>{
                reject(error)
            })
        },2000)
    })
}

module.exports = getDetails