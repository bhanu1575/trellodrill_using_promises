/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json 
    and then pass control back to the code that called it by using a callback function.
*/

function getBoardInformation(boards,id){
    return new Promise((resolve,reject)=>{
        setTimeout(()=>{
            let result = boards.find((board) => board.id === id)
            if(result === undefined) {
                reject('Invalid ID / No Board was found with this given ID.')
            }
            resolve(result)
        },2000)
    })
}


module.exports = getBoardInformation